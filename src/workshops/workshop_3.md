# Workshop 3: Verbinden en georefereren van Erfgoed

Het beeldmateriaal dat door de Bibliotheek UvA/HvA als Linked Open Data beschikbaar is gesteld, heeft raakvlakken met andere datasets die nog niet expliciet gemaakt zijn. [Geportretteerden](https://lod.uba.uva.nl/UB-UVA/Beeldbank) kunnen gelinkt worden aan biografische databases; plaatsen, straten en gebouwen aan geografische datasets. De mogelijkheden zijn legio: denk alleen al aan [Adamlink](https://adamlink.nl/), [Collectie Nederland](https://data.collectienederland.nl/snorql/), [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page) en de [Referentielijst Verdwenen Gebouwen](https://verdwenengebouwen.nl/). In deze sessie onderzoeken we welke mogelijkheden er zijn om de Linked Open Data van de Bibliotheek UvA/HvA op een zinvolle manier met dergelijke datasets te verbinden en te verrijken. Daarbij kijken we ook naar beschikbare APIs en programmatuur waarmee deze verbindingen tot stand kunnen worden gebracht.

~~~ admonish tip "Feedback?"
Tijdens de workshop werken we met een eerste vroege versie van de LOD van de UB. Heb je foutjes gezien die verbeterd moeten worden? Moet het datamodel uitgebreid worden, of heb je andere opmerkingen? Laat het ons weten via een [gedeeld online document](https://amsuni-my.sharepoint.com/:w:/g/personal/i_m_m_vanderknaap_uva_nl/EYaSg6v2XXxPqb2wHQrhxKgBbap9CnmCc3REIph17rfocw?CID=981a494a-f270-eac5-6631-d9cf88aa8dff). Dit is bedoeld als feedback op de data. Resultaten en query's kunnen op deze pagina's gepresenteerd worden!
~~~

## Erfgoedcollecties linken met externe identifiers

We wilden een data over meerdere endpoints bevragen (kracht van linked data immers), maar kwamen er achter dat er nog weinig data verbonden was met externe identifiers.

Dat zijn we dus zelf gaan doen.

### Stap 1: data analyse en download

Deze sparql query op de [UB beeldbank](https://lod.uba.uva.nl/UB-UVA/Beeldbank/sparql/virtuoso) leverde ons 397 geportretteerden:

```sparql
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT * WHERE {
  ?sub dc:type "portret" .
  ?sub rdfs:label ?label .
} LIMIT 1000
```

Die hebben we gedownload als [csv](scripts/geportretteerden.csv)

### Stap 2: alignen

Met [dit script](scripts/parse.php) hebben we met een reguliere expressie de naam en de geboorte- en sterfjaren uit die csv getrokken (nog niet in alle gevallen gelukt he, de tijdsdruk was enorm!) en die gegevens vervolgens tegen Wikidata aangehouden.

Daar verkregen we het bestand [matches.csv](scripts/matches.csv) uit terug.

### Stap 3: turtle maken

Met het script [export-turtle.php](scripts/export-turtle.php) hebben we daar turtle van gemaakt, en Heidi heeft dat binnen een minuut in de Triply endpoint opgenomen.

### Stap 4: federated query

Nu kunnen we vragen over verschillende endpoints maken, bijvoorbeeld deze:

```sparql
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/>

SELECT * WHERE {
  ?sub dc:type "portret" .
  ?sub rdfs:label ?label .
  ?sub foaf:depicts ?geportretteerde .
  SERVICE <https://query.wikidata.org/sparql> {
        ?geportretteerde wdt:P19 ?pob .
    	?pob wdt:P625 ?coords .
    }
} LIMIT 1000
```