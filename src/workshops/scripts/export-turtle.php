<?php

$prefix = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n";
$prefix .= "PREFIX wd: <http://www.wikidata.org/entity/>\n\n";

echo $prefix;


if (($handle = fopen("matches.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

    	//print_r($data);
    	if($data[0] == "handle"){
    		continue;
    	}

    	echo "<" . $data[0] . "> foaf:depicts wd:" . str_replace("http://www.wikidata.org/entity/","",$data[2]) . " .\n";
	}
}
?>