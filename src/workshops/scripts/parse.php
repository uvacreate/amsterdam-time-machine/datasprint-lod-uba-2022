<?php


include("functions.php");

$fp = fopen('matches.csv', 'w');
$columnnames = array("handle","label","wd item","wd label","wd description","wd geb. jaar", "wd sterfjaar");
fputcsv($fp, $columnnames);
fclose($fp);

if (($handle = fopen("../geportretteerden.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

        print_r($data);

        if(preg_match("/^([^,]+), ([^\(]+) \(([^\)]+)\) \(([0-9]{4})-([0-9]{4})\)/",$data[1],$found)){
			print_r($found);
			$naam = $found[3] . " " . $found[1];
			$yob = $found[4];
			$yod = $found[5];
		}else{
			continue;
		}

        if($wd = findwiki($naam,$yob,$yod)){

        	//print_r($wd);
        	$rec = array(
        		$data[0],
        		$data[1],
        		$wd['item']['value'],
        		$wd['itemLabel']['value'],
        		$wd['itemDescription']['value'],
        		$wd['yob']['value'],
        		$wd['yod']['value']
        	);

        	$fp = fopen('matches.csv', 'a');
			fputcsv($fp, $rec);
			fclose($fp);
		
		}


    }
    fclose($handle);
}


die;


function findwiki($naam,$yob,$yod){

	
	$sparql = '
	SELECT  ?num ?item ?itemLabel ?itemDescription ?yob ?yod
	WHERE { 
	  SERVICE wikibase:mwapi {
	    bd:serviceParam wikibase:api "EntitySearch" ;
	                    wikibase:endpoint "www.wikidata.org" ;
	                    mwapi:search "' . $naam . '" ;
	                    mwapi:language "nl" . 
	    ?item wikibase:apiOutputItem mwapi:item . 
	    ?num wikibase:apiOrdinal true . 
	  } 
	  ?item wdt:P569 ?dob  . 
	  ?item wdt:P570 ?dod .
      BIND (year(?dob) AS ?yob)
      BIND (year(?dod) AS ?yod)
      FILTER(?yob = ' . $yob . ')
      FILTER(?yod = ' . $yod . ')
      SERVICE wikibase:label { bd:serviceParam wikibase:language "nl,en". }
	} 
	ORDER BY ASC(?num)
	LIMIT 10';
	
	//echo $sparql;

	$endpoint = 'https://query.wikidata.org/sparql';

	$json = getSparqlResults($endpoint,$sparql);
	$data = json_decode($json,true);

	//print_r($data);

	foreach($data['results']['bindings'] as $res){

		//if($res['num']['value'] == 0 && levenshtein($res['itemLabel']['value'], $person['naam'])<2){
		if($res['num']['value'] == 0){

			return $res;

		}


	}

	return false;

}








?>