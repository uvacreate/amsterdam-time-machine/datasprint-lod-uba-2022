# Aan de slag

- [Aan de slag](#aan-de-slag)
  - [Linked (Open) Data](#linked-open-data)
  - [Queryen: SPARQL](#queryen-sparql)
  - [Datasets](#datasets)
    - [Data van de UB UvA](#data-van-de-ub-uva)
    - [Andere datasets](#andere-datasets)
      - [Wikidata](#wikidata)
      - [CREATE](#create)
      - [Koninklijke Bibliotheek](#koninklijke-bibliotheek)
      - [Collectie Nederland](#collectie-nederland)
      - [Netwerk Digitaal Erfgoed](#netwerk-digitaal-erfgoed)


## Linked (Open) Data

> Bij linked open data (LOD) wordt data op zo’n manier aangeboden dat er veel makkelijker en automatisch verbinding gelegd kan worden met andere data. Hierdoor wordt de data rijker en nuttiger voor de gebruiker.

Afkomstig van: [https://www.nationaalarchief.nl/onderzoeken/linked-open-data](https://www.nationaalarchief.nl/onderzoeken/linked-open-data)

De [Programming Historian](https://programminghistorian.org/en/lessons/intro-to-linked-data) heeft ook een introductie over LOD. Verder kun je zien wat je met Linked Data kunt doen in [een artikel](https://hcommons.org/deposits/objects/hc:41776/datastreams/CONTENT/content#page=47) dat verschillende datasets met historische data gebruikt voor onderzoek naar canonvorming in de 17e en 18e eeuw.

Zie ook de uitleg (inclusief een video en wat voorbeelden) van het Netwerk Digitaal Erfgoed (NDE) ([https://netwerkdigitaalerfgoed.nl/activiteiten/linked-data/](https://netwerkdigitaalerfgoed.nl/activiteiten/linked-data/)) als je meer wilt weten. Verder zijn er talloze handleidingen op het internet te vinden om je op weg te helpen. 

## Queryen: SPARQL

De [Wikidata Query Tutorial](https://www.wikidata.org/wiki/Wikidata:SPARQL_tutorial) kan je op weg helpen om te leren SPARQL'en, al is deze wel erg gericht op de query service en syntax van Wikidata. Voor een algemenere (maar ook technischere) introductie kunnen we je [_Learning SPARQL_ van Bob DuCharme](http://www.learningsparql.com/) aanraden.  

Vaak is het makkelijk om te beginnen met een kleine voorbeeldquery om die vervolgens langzaam uit te breiden. Bij elk statement dat je toevoegt kun je zo controleren of de query correct is. 

Twee voorbeeldquery's zijn bijvoorbeeld:

~~~ admonish example "Alle cartografen in Ecartico"
```sparql
#+ endpoint: https://data.create.humanities.uva.nl/sparql
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX schema: <http://schema.org/>

SELECT * WHERE {
  GRAPH <https://data.create.humanities.uva.nl/id/ecartico/> {
    ?person a schema:Person ;
      schema:name ?name ;
      schema:hasOccupation/schema:hasOccupation <https://www.vondel.humanities.uva.nl/ecartico/occupations/40> . # Cartographer       
  }
}
```
~~~

~~~ admonish example "Alle kaarten in de UB van voor 1740"
```sparql
#+ endpoint: https://api.lod.uba.uva.nl/datasets/UB-UVA/Maps/services/Maps/sparql
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
SELECT (count(DISTINCT ?sub) AS ?count) WHERE {
  ?sub dc:type <http://vocab.getty.edu/aat/300028094> ;
       dc:date ?date.
  filter(regex(?date, "((15|16)[0-9][0-9]|17[0-3])", "i" )) 
} LIMIT 100
```
~~~
## Datasets

### Data van de UB UvA

> De collecties die de Bibliotheek beheert lopen uiteen van vakgebied-specifieke boeken- en tijdschriftencollecties, archieven van organisaties en personen, en internationaal vermaarde erfgoedcollecties (op het gebied van archeologie, boekgeschiedenis, cartografie, grafische vormgeving, Joodse cultuurgeschiedenis, natuurlijke historie, literatuur en uitvoerende kunsten).
> 
> [...]
> 
> De data die bij deze collecties horen worden beschikbaar gesteld in op maat gesneden formats en via diverse publicatiekanalen zoals downloads, OAI harvesting, API's, Linked Data Endpoints en andere tools.
> 
> Voor de collectiemetadata van de Bibliotheek geldt standaard een PDDL Public Domain licentie. Een uitzondering geldt voor de metadata die oorspronkelijk afkomstig is uit OCLC WorldCat. Hiervoor geldt een ODC-BY licentie op basis van de WorldCat Community Norms.

Afkomstig van: [https://uba.uva.nl/ondersteuning/open-data/open-data.html](https://uba.uva.nl/ondersteuning/open-data/open-data.html)

### Andere datasets

#### Wikidata

In Wikidata zijn gegevens van o.a. Wikipedia opgenomen als Linked Data en bevat data uit allerlei domeinen. Alles wat je kunt bedenken kan een plek krijgen en beschreven worden in Wikidata. Het gebruik van Wikidata wordt ook steeds populairder bij erfgoedinstellingen. Bovendien kun je Wikidata goed gebruiken als 'hub' voor gelinkte data. Zo aggregeert het bijvoorbeeld veel identifiers van de beschreven resource naar andere datasets. 

Startpunt om iets te vinden in Wikidata is: [https://wikidata.org/](https://wikidata.org/). SPARQL'en kan via: [https://query.wikidata.org/](https://query.wikidata.org/)

#### CREATE

CREATE ([https://www.create.humanities.uva.nl/](https://www.create.humanities.uva.nl/)) host een aantal datasets in een SPARQL-endpoint, waaronder:

* **Adamlink**: Referentiedata voor Amsterdamse collecties. Zie: [https://www.adamlink.nl/](https://www.adamlink.nl/)
* **Cinema Context**: Online Encyclopedie van de filmcultuur in Nederland vanaf 1896. Zie: [https://www.cinemacontext.nl/](https://www.cinemacontext.nl/) en voor een documentatie van deze dataset in RDF: [https://uvacreate.gitlab.io/cinema-context/cinema-context-rdf/](https://uvacreate.gitlab.io/cinema-context/cinema-context-rdf/)
* **ECARTICO**: Comprehensive collection of structured biographical data concerning painters, engravers, printers, book sellers, gold- and silversmiths and others involved in the ‘cultural industries’ of the Low Countries in the sixteenth and seventeenth centuries. [https://www.vondel.humanities.uva.nl/ecartico/](https://www.vondel.humanities.uva.nl/ecartico/)
* **ONSTAGE**: Online Datasystem of Theatre in Amsterdam from the Golden Age to the present. Zie [https://www.vondel.humanities.uva.nl/onstage/](https://www.vondel.humanities.uva.nl/onstage/)
* **Schrijverskabinet-RDF**: Verzameling portretten van Nederlandse dichters. Zie: [http://schrijverskabinet.nl/](http://schrijverskabinet.nl/)
* **Notarissennetwerk-RDF**: Het Notarissen Netwerk is een bewerking van het Repertorium van (Amsterdamse) Notarissen. Zie: [https://notarissennetwerk.nl/](https://notarissennetwerk.nl/)

De datasets zijn hier inzichtelijk en te queryen: [https://data.create.humanities.uva.nl/](https://data.create.humanities.uva.nl/)


#### Koninklijke Bibliotheek

De KB heeft haar datasets gepubliceerd op [http://data.bibliotheken.nl/](http://data.bibliotheken.nl/) met bijbehorend SPARQL-endpoint: [http://data.bibliotheken.nl/sparql](http://data.bibliotheken.nl/sparql). 

Op dit moment zijn de datasets:

| Dataset                                                                   | URI                                                          |
| ------------------------------------------------------------------------- | ---------------------------------------------------------    |
| Alba amicorum van de Koninklijke Bibliotheek                              | [`http://data.bibliotheken.nl/id/dataset/rise-alba`](http://data.bibliotheken.nl/id/dataset/rise-alba)             |
| Brinkman trefwoordenthesaurus                                             | [`http://data.bibliotheken.nl/id/dataset/brinkman`](http://data.bibliotheken.nl/id/dataset/brinkman)              |
| Centsprenten                                                              | [`http://data.bibliotheken.nl/id/dataset/rise-centsprenten`](http://data.bibliotheken.nl/id/dataset/rise-centsprenten)     |
| Gemeenschappelijke Trefwoordenthesaurus (GTT)                             | [`http://data.bibliotheken.nl/id/dataset/gtt`](http://data.bibliotheken.nl/id/dataset/gtt)                   |
| Nederlandse Bibliografie Totaal (NBT)                                     | [`http://data.bibliotheken.nl/id/dataset/nbt`](http://data.bibliotheken.nl/id/dataset/nbt)                   |
| Organisaties uit de corporatiethesaurus van de Koninklijke Bibliotheek    | [`http://data.bibliotheken.nl/id/dataset/corps`](http://data.bibliotheken.nl/id/dataset/corps)                 |
| Personen uit de Nederlandse Thesaurus van Auteursnamen (NTA)              | [`http://data.bibliotheken.nl/id/dataset/persons`](http://data.bibliotheken.nl/id/dataset/persons)               |
| Short-Title Catalogue Netherlands (STCN)                                  | [`http://data.bibliotheken.nl/id/dataset/stcn`](http://data.bibliotheken.nl/id/dataset/stcn)                  |
| Thesaurus Auteurs DBNL                                                    | [`http://data.bibliotheken.nl/id/dataset/dbnla`](http://data.bibliotheken.nl/id/dataset/dbnla)                 |
| Thesaurus KBcode                                                          | [`http://data.bibliotheken.nl/id/dataset/kbcode`](http://data.bibliotheken.nl/id/dataset/kbcode)                |
| Titels DBNL                                                               | [`http://data.bibliotheken.nl/id/dataset/dbnlt`](http://data.bibliotheken.nl/id/dataset/dbnlt)                 |


#### Collectie Nederland

De Collectie Nederland brengt data van musea en andere cultuurinstellingen bijeen volgens het Europeana datamodel. Data zijn hier inzichtelijk: [https://data.collectienederland.nl/](https://data.collectienederland.nl/) en er is een bijbehorend SPARQL-endpoint: [https://data.collectienederland.nl/snorql](https://data.collectienederland.nl/snorql).

#### Netwerk Digitaal Erfgoed

Meer datasets zijn te vinden in het Datasetregister van het Netwerk Digitaal Erfgoed: [https://datasetregister.netwerkdigitaalerfgoed.nl/](https://datasetregister.netwerkdigitaalerfgoed.nl/).

