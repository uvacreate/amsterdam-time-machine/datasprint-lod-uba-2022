# Summary
# Inhoud
- [Introductie](./README.md)

- [Aan de slag](./getting-started.md)

- [Workshops](./workshops/README.md)
  - [Workshop 1: Kaartenproductie in de zeventiende eeuw](./workshops/workshop_1.md)
  - [Workshop 2: Culinaire cultuur in beweging](./workshops/workshop_2.md)
  - [Workshop 3: Verbinden en georefereren van Erfgoed](./workshops/workshop_3.md)
  - [Workshop 4 (rondetafelgesprek): Archeologische collectie Allard Pierson](./workshops/workshop_4.md)
