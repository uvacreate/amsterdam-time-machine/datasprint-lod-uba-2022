# Introductie

_Op donderdag 9 juni kunnen onderzoekers, informatiespecialisten en erfgoed-/bibliotheek-professionals tijdens een datasprint kennismaken met de gebruiksmogelijkheden van de datacollecties en Linked Open Data van de Bibliotheek UvA/HvA._

Een groot deel van de collectiedatabases van de Bibliotheek UvA/HvA zijn als [Linked Open Data (LOD)](https://uba.uva.nl/ondersteuning/open-data/open-data.html) beschikbaar gesteld. De datacollecties van de Bibliotheek zijn nu beter te onderzoeken en kunnen bovendien worden verbonden aan andere datasets. Welke nieuwe mogelijkheden levert dit op voor onderzoek? En hoe kunnen de aangeboden data verbeterd en verrijkt worden?

Tijdens deze datasprint werken deelnemers in parallelle workshops aan een thema. Er is een GitLab repository beschikbaar om tijdens de workshops data en scripts te delen en interessante resultaten en visualisaties te presenteren. Hoewel het deelnemers vrijstaat om eigen sessies te organiseren (zelfs eigen data mee te nemen), zijn er ook vier workshops voorbereid.

**Update:** <br>
We zijn positief verrast door het hoge aantal aanmeldingen. Hierdoor gaan we uitwijken naar een andere locatie met een grotere zaal. Deze locatie is de Roeterseiland Campus van de UvA, Nieuwe Achtergracht 166, Amsterdam. De datasprint vindt plaats in zaal B3.09. Om hier te komen neem de ingang B/C (zie voor de locatie het kaartje in deze link). Bij binnenkomst volg de pijlen naar ‘B’ en/of loop rechtdoor de hal in. Ga aan het einde van de hal de gang links in. Ga aan het einde van de gang naar rechts, hier zie je de liften. Neem de lift naar de 3e verdieping. Loop vanuit de lift rechtdoor en sla links de hoek om, waar je zaal B3.09 vindt. Mocht je het niet kunnen vinden kan je om hulp vragen bij de receptie bij de ingang B/C.



<!-- toc -->

## Details

| Informatie |                                                      |
| ---------- | ---------------------------------------------------- |
| Datum      | 9 juni 2022                                          |
| Tijd       | 13.00 - 17.00 uur                                    |
| Locatie    | ~~E-lab BG1 0.16, <br> Turfdraagsterpad 9, 1012 XT Amsterdam~~ <br> REC B3.09 <br> Nieuwe Achtergracht 166, 1018 WV Amsterdam <br> [route](https://www.uva.nl/locaties/roeterseiland/rec-b-c-d-ingang-b-c.html) |

### Programma

| Tijd              | Omschrijving                   |
| ----------------- | ------------------------------ |
| 13.00 - 13.20 uur | Inloop                         |
| 13:20 - 13:30 uur | Welkom en introductie          |
| 13.30 - 16.15 uur | Werken aan de data             |
| 16.15 - 17.00 uur | Korte presentatie per workshop |
| 17.00 uur         | Borrel                         |

## Parallelle worskhops

### Workshop 1: Kaartenproductie in de zeventiende eeuw

Amsterdam was in de zeventiende eeuw het belangrijkste productiecentrum van geografische kaarten ter wereld. Blaeu en Janssonius zijn nog altijd bekende namen, ook buiten de kring van specialisten op het gebied van de historische cartografie. Veel van wat destijds is geproduceerd, is terug te vinden in de [cartografische collectie van de Bibliotheek UvA/HvA](https://lod.uba.uva.nl/UB-UVA/Maps/). In deze sessie verbinden we de metadata van deze collectie met de aan de UvA ontwikkelde [Ecartico database](https://www.vondel.humanities.uva.nl/ecartico/) waarin data zijn te vinden over producenten van kaarten en hun sociale netwerken. Zo hopen we meer inzicht te krijgen in hoe samenwerking en concurrentie in de zeventiende-eeuwse cartografie gestalte kregen. Vervolgens bekijken we of het mogelijk en zinvol is om andere data aan dit amalgaam toe te voegen. Denk bijvoorbeeld aan de collectiedatabase van het Rijksmuseum, of de open data van de Koninklijke Bibliotheek.

### Workshop 2: Culinaire cultuur in beweging

Van Balkenbrij tot Big Mac: onze eetcultuur is divers en uitermate dynamisch. De [kookboekencollectie van de Bibliotheek UvA/HvA](https://lod.uba.uva.nl/UB-UVA/Collectie-Geschiedenis-van-de-Voeding/) biedt een inkijkje in hoe onze eetgewoontes zich de afgelopen eeuwen ontwikkeld hebben. In deze sessie onderzoeken we hoe we de metadata van deze collectie kunnen gebruiken om ontwikkelingen en patronen in onze culinaire cultuur bloot te leggen. In welke perioden werd bijvoorbeeld het vegetarisme gepropageerd? Of hoe verliep de opkomst van de Italiaanse keuken in de Lage Landen? Uiteraard zijn bij het beantwoorden van dergelijke vragen ook andere bronnen en datasets te gebruiken. Denk hierbij aan de [kookboekencollectie van de KB](https://www.kb.nl/onderzoeken-vinden/bijzondere-collecties/kookboeken), [Collectie Nederland](https://data.collectienederland.nl/snorql/), de [n-gramviewers van DBNL](https://www.dbnl.org/ngram-viewer/) en [Google](https://books.google.com/ngrams/graph?content=vegetarianism&year_start=1800&year_end=2016&corpus=15&smoothing=3&direct_url=t1%3B%2Cvegetarianism%3B%2Cc0#t1%3B%2Cvegetarianism%3B%2Cc0), [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page) en het [Chronologisch Woordenboek van Nicoline van der Sijs](https://dbnl.org/tekst/sijs002chro01_01/) waarin lijsten zijn opgenomen met culinaire leenwoorden uit onder andere het Italiaans en het Frans.

### Workshop 3: Verbinden en georefereren van Erfgoed

Het beeldmateriaal dat door de Bibliotheek UvA/HvA als Linked Open Data beschikbaar is gesteld, heeft raakvlakken met andere datasets die nog niet expliciet gemaakt zijn. [Geportretteerden](https://lod.uba.uva.nl/UB-UVA/Beeldbank) kunnen gelinkt worden aan biografische databases; plaatsen, straten en gebouwen aan geografische datasets. De mogelijkheden zijn legio: denk alleen al aan [Adamlink](https://adamlink.nl/), [Collectie Nederland](https://data.collectienederland.nl/snorql/), [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page) en de [Referentielijst Verdwenen Gebouwen](https://verdwenengebouwen.nl/). In deze sessie onderzoeken we welke mogelijkheden er zijn om de Linked Open Data van de Bibliotheek UvA/HvA op een zinvolle manier met dergelijke datasets te verbinden en te verrijken. Daarbij kijken we ook naar beschikbare APIs en programmatuur waarmee deze verbindingen tot stand kunnen worden gebracht.

### Workshop 4 (rondetafelgesprek): Archeologische collectie Allard Pierson

Ook metadata van de [archeologische collectie van het Allard Pierson](https://lod.uba.uva.nl/UB-UVA/Archaeological-Objects) zijn als Linked Open Data beschikbaar gemaakt. Meer dan bij andere Linked Open Data sets van de Bibliotheek UvA/HvA, speelt hier de uitdaging van de enorme diversiteit van de beschreven objecten. Dit vormt het vertrekpunt van een rondetafelgesprek over hoe de bruikbaarheid van deze data geoptimaliseerd kan worden voor onderzoek. Is deze data nu al bruikbaar voor archeologen? En hoe (goed) is deze data in te passen in een groter web van archeologische data?

## Over CREATE

Het onderzoeksprogramma [‘Creative Amsterdam: An E-Humanities Perspective’ (CREATE)](https://www.create.humanities.uva.nl/) van het Amsterdam Centre for Cultural Heritage and Identity verbindt onderzoek naar cultuur, identiteit en geschiedenis met digitale methoden en technieken. Het programma onderzoekt hoe culturele industrieën de unieke positie van Amsterdam in een Europese en mondiale context hebben gevormd, van de zeventiende eeuw tot nu.

![CREATE](./images/create-logo.png)
