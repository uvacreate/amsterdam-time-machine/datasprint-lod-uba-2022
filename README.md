# Datasprint Linked Open Data UB-UvA and CREATE

In deze repository staan de bestanden waarmee de bijbehorende GitLab pages ([https://uvacreate.gitlab.io/amsterdam-time-machine/datasprint-lod-uba-2022/](https://uvacreate.gitlab.io/amsterdam-time-machine/datasprint-lod-uba-2022/)) gebouwd worden. 

Elke keer dat er een wijziging aan de repository gecommit wordt, gaat er een pipeline aan de slag die de pagina's zo'n 5-10 minuten later zichtbaar maakt. Deze pagina's zijn opgemaakt in markdown (zie: [https://www.markdownguide.org/basic-syntax/](https://www.markdownguide.org/basic-syntax/)), maar ondersteunen ook basic html, mocht dat nodig zijn. 

Het idee hiervan is dat er vooraf, tijdens én na de datasprint een documentatiewebsite beschikbaar is waarop de resultaten en bevindingen van deze middag terug te lezen zijn. 

## Wijzigingen aanbrengen?

Wijzigingen/toevoegingen mogen ingediend worden via een pull-request, of kunnen direct in de repository gemaakt worden als je toegang hebt (vraag hiernaar tijdens de datasprint). Weet je niet hoe? Dan kun je ook een issue aanmaken of een e-mail sturen naar createlab@uva.nl. Dan verwerken wij de wijzigingen!

## Licentie

In principe is alle documentatie beschikbaar onder een [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) licentie. Dit kan anders zijn voor (ingesloten) databestanden en is zeker anders voor data afkomstig van OCLC-gerelateerde diensten. Let hierop als je iets publiceert/deelt. 

## Contact

[createlab@uva.nl](mailto:createlab@uva.nl)
